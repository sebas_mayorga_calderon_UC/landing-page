import { TestBed, inject } from '@angular/core/testing';

import { InformtionService } from './informtion.service';

describe('InformtionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InformtionService]
    });
  });

  it('should be created', inject([InformtionService], (service: InformtionService) => {
    expect(service).toBeTruthy();
  }));
});

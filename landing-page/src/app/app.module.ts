import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {InMemoryDataService} from "./DataManagement/InMemoryDataService";
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";
import {InformtionService} from "./informtion.service";
import {HttpClientModule} from "@angular/common/http";
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ListaIntegrantesComponent } from './lista-integrantes/lista-integrantes.component';
import { IntegranteComponent } from './lista-integrantes/integrante/integrante.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import { ValoresComponent } from './valores/valores.component';
import { ReglamentoComponent } from './reglamento/reglamento.component';

@NgModule({

  declarations: [
    AppComponent,
    NavBarComponent,
    ListaIntegrantesComponent,
    IntegranteComponent,
    DashboardComponent,
    ValoresComponent,
    ReglamentoComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {
      dataEncapsulation: false,
      delay: 300,
      passThruUnknownUrl: true
    })
  ],
  providers: [InformtionService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

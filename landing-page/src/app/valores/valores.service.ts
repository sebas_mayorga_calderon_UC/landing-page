import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValoresService {
 valores = [
   {valor: 'Solidaridad', desc:'brinda al equipo un sentimiento de unidad, siempre en\n' +
     'busca del bienestar de todos los integrantes. Universal Code aplica este\n' +
     'valor ya que somos un equipo por lo tanto si uno cae todos caemos,\n' +
     'necesitamos estar unidos y dándonos apoyo mutuo para que todos\n' +
     'vayamos al mismo ritmo, así como ayudar a los que no tienen tanta\n' +
     'habilidad para que no queden atrás.'},
   {valor: 'Entusiasmo', desc: 'es lo que permite que una persona de todo de sí misma\n' +
     'para lograr sus objetivos. Para Universal Code es importante que sus\n' +
     'integrantes estén completamente comprometidos con el proyecto. De\n' +
     'esta forma, se puede lograr el objetivo de manera satisfactoria.'},
   {valor: 'Confianza', desc: 'es la seguridad que alguien tiene en otra persona o en algo.\n' +
     'Universal Code considera que la confianza entre sus integrantes es muy\n' +
     'importante a lo hora de realizar entregables, ya que a la hora de dividir\n' +
     'el trabajo se está confiando en los compañeros para que hagan una\n' +
     'labor de calidad.'},
   {valor: 'Comunicación', desc: 'constante es la clave para evitar confusiones o\n' +
     'problema entre los miembros de Universal Code. Si se tienen conflictos\n' +
     'menores, la mejor manera de evitar que se conviertan en problemas\n' +
     'mayores, es hablando sobre la situación como equipo para tratar de\n' +
     'encontrar una solución.'},
   {valor: 'Trabajo en equipo', desc: 'a la hora de desarrollar el producto. Es una manera\n' +
     'muy simple de asegurar buenas relaciones entre el equipo y facilidad a la hora de desarrollar entregables. De esta forma se puede llegar\n' +
     'satisfactoriamente al objetivo planteado. Universal Code busca hacer\n' +
     'bien las cosas, perfeccionando y mejorando en lo posible, tanto el\n' +
     'proceso como el producto final, es por eso que se considera la\n' +
     'orientación a la calidad como algo muy importante.'}
 ];
  getValores(){
    return this.valores.slice();
  }
  constructor() { }
}

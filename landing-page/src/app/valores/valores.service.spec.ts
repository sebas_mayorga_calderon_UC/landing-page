import { TestBed, inject } from '@angular/core/testing';

import { ValoresService } from './valores.service';

describe('ValoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValoresService]
    });
  });

  it('should be created', inject([ValoresService], (service: ValoresService) => {
    expect(service).toBeTruthy();
  }));
});

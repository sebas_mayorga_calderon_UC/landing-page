import { Component, OnInit } from '@angular/core';
import {ValoresService} from './valores.service';

@Component({
  selector: 'app-valores',
  templateUrl: './valores.component.html',
  styleUrls: ['./valores.component.css']
})
export class ValoresComponent implements OnInit {

  constructor( private valoresService: ValoresService) { }
  valores;
  ngOnInit() {
    this.valores = this.valoresService.getValores();
  }

}

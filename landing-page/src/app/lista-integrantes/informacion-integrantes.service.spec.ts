import { TestBed, inject } from '@angular/core/testing';

import { InformacionIntegrantesService } from './informacion-integrantes.service';

describe('InformacionIntegrantesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InformacionIntegrantesService]
    });
  });

  it('should be created', inject([InformacionIntegrantesService], (service: InformacionIntegrantesService) => {
    expect(service).toBeTruthy();
  }));
});

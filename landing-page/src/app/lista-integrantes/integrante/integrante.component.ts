import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-integrante',
  templateUrl: './integrante.component.html',
  styleUrls: ['./integrante.component.css']
})
export class IntegranteComponent implements OnInit {
  @Input() integrante;
  constructor() { }

  ngOnInit() {
  }

}

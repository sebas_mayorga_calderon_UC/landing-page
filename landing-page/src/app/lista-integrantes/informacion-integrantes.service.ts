import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InformacionIntegrantesService {
  integrantes = [
    {
      name: 'Sebastian Mayorga Calderon',
      rol: 'Desarollo',
      tel: '8860-2403',
      correos: ['sebasmayorga7@gmail.com', 'smayorgac@@ucenfotec.ac.cr'],
      tecnologias: ['IntelliJ', 'Webstrorm', 'HeidySQL', 'Postman'],
      image: '../../assets/images/Sebas.jpg'
    },
    {
      name: 'Maria Jaimes',
      rol: 'Coordinador',
      tel: '6030-1390',
      correos: ['mariajaimesv@gmail.com', 'mjaimesv@ucenfotec.ac.cr'],
      tecnologias: ['Trello', 'Slack'],
      image: '../../assets/images/Maria.jpg'
    },
    {
      name: 'Mario Torres Ramirez',
      rol: 'Soporte',
      tel: '8942-6184',
      correos: ['mariotr89@gmail.com', 'mtorresr@ucenfotec.ac.cr'],
      tecnologias: ['Git', 'Kadiff3', 'Sourcetree'],
      image: '../../assets/images/Mario.jpg'
    },
    {
      name: 'Melanie Fallas',
      rol: 'Calidad',
      tel: '7203-4350',
      correos: ['melafallas@gmail.com', 'mfallasr@ucenfotec.ac.cr'],
      tecnologias: ['Jmeter', 'Junit'],
      image: '../../assets/images/Melanie.jpg'
    }
  ];

  constructor() {
  }

  getIntegrantes = () => {
    return this.integrantes.slice();
  }
}

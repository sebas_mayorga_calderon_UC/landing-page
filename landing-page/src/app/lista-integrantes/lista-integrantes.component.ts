import { Component, OnInit } from '@angular/core';
import {InformacionIntegrantesService} from './informacion-integrantes.service';

@Component({
  selector: 'app-lista-integrantes',
  templateUrl: './lista-integrantes.component.html',
  styleUrls: ['./lista-integrantes.component.css']
})
export class ListaIntegrantesComponent implements OnInit {
  integrantes;
  constructor(private informationService: InformacionIntegrantesService) { }

  ngOnInit() {
    this.integrantes = this.informationService.getIntegrantes();
  }

}

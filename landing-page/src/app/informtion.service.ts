import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {InformationObj} from "./InformationObj";
import {map} from "rxjs/operators";

@Injectable()
export class InformtionService {
  private infomrationUrl = 'app/information'; // URL to web api

  constructor(private http: HttpClient) {
  }

  getInformation() {
    return this.http
      .get<InformationObj[]>(this.infomrationUrl)
      .pipe(map(data => data));
  }
}

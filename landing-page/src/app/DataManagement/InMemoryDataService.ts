export class InMemoryDataService {
  createDb() {
    const information =
      [{
        tittle: "Descripción",
        desc: 'Universal Code busca hacer bien las cosas, perfeccionando y mejorando en lo posible, tanto el proceso como el producto final, es por eso que se considera la orientación a la calidad como algo muy importante.'
      }, {
        tittle: "Misión",
        desc: 'La misión de Universal Code es ofrecer soluciones tecnológicas e innovadoras,\n' +
        'adaptables a las necesidades de nuestros clientes, fomentando su desarrollo y\n' +
        'crecimiento mediante un equipo altamente competitivo.'
      },
        {
        tittle: "Visión",
        desc: 'Seguiremos construyendo nuestro futuro siendo un equipo responsable y\n' +
        'competitivo que ofrece servicios de calidad en tecnología. Nuestro objetivo es\n' +
        'superarnos cada día y demostrar nuestras capacidades.'
      }]
    return {information};
  }
}



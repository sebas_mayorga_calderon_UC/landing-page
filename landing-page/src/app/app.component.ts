import {Component, OnInit} from '@angular/core';
import {InformtionService} from "./informtion.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Universal Code';
  information = {};

  constructor(
    private informationService: InformtionService) {
  }

  ngOnInit(): void {
    this.informationService.getInformation()
      .subscribe((a) => this.information = a);
  }
}

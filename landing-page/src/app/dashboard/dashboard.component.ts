import { Component, OnInit } from '@angular/core';
import {InformtionService} from '../informtion.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  title = 'Universal Code';
  information = [];

  constructor(
    private informationService: InformtionService) {
  }

  ngOnInit(): void {
    this.informationService.getInformation()
      .subscribe((a) => this.information = a);
  }
}

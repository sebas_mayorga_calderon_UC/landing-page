import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ListaIntegrantesComponent} from './lista-integrantes/lista-integrantes.component';
import {ValoresComponent} from './valores/valores.component';
import {ReglamentoComponent} from './reglamento/reglamento.component';




const Routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'valores', component: ValoresComponent },
  { path: 'reglamento', component: ReglamentoComponent },
  { path: 'integrantes', component: ListaIntegrantesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(Routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

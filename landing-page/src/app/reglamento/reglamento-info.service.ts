import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReglamentoInfoService {
  puntos = [
    {titulo: 'Sobre las entregas' , subPuntos: ['Entregar los documentos de forma puntual, hora y fecha que se establezca; en caso de no poder cumplir con lo anterior la persona debe avisar mínimo\n' +
      '3 horas antes de la establecida y de llegarse a un acuerdo grupal por\n' +
      'mayoría se le darán 12 horas más para cumplir con la entrega sin anotar el\n' +
      'incumplimiento en la minuta.', 'Si la persona no entrega a tiempo o no se cumple con el requisito en el\n' +
      'punto anterior, se anotará la falta.', 'Si la persona no entrega, se le asignará a algún compañero que ya tenga\n' +
      'sus entregas listas, y al compañero que no entregó se le quitaran los puntos\n' +
      'correspondientes.']},
    {titulo: 'Sobre las reuniones', subPuntos: ['Se realizará mínimo una reunión presencial por semana, los días martes o\n' +
      'viernes, según se amerite. El lugar por defecto será la Universidad\n' +
      'Cenfotec.', 'Reuniones virtuales: se organizarán cada vez que sea necesario.', 'En caso de no poder asistir a la reunión presencial o virtual, la persona\n' +
      'debe avisar con 1 hora de antelación como mínimo y tener una razón\n' +
      'válida, aprobada por la mayoría del grupo.', 'De no justificar la ausencia en una reunión o si no se cumple el punto\n' +
      'mencionado anteriormente, se anotará el incumplimiento en la minuta.']},
    {titulo: 'Discusiones y faltas de respeto', subPuntos: ['En caso de que una discusión se salga de lo normal, se tratará de mediar\n' +
      'con la coordinadora del grupo (María). De no solucionarse el conflicto se\n' +
      'llevará el tema a discusión con el profesor Romero o Cordero.', 'Si el conflicto se da con la coordinadora, y no se resuelve en primera línea,\n' +
      'se procede a involucrar al profesor de igual forma que en el punto anterior.', 'En caso que se dé alguna falta de respeto de cualquier naturaleza, se\n' +
      'amonestará a la persona que la ocasione de forma inmediata al llegar a un\n' +
      'acuerdo con la mayoría del grupo y se anotará en la minuta.']},
    {titulo: 'Amonestaciones', subPuntos: ['Al darse 3 faltas de los puntos mencionados con respecto a reuniones y\n' +
      'entregas, se procederá a amonestar al integrante de forma inmediata con el\n' +
      'profesor del área correspondiente.', 'Cada falta leve se bajarán 5 puntos y cada falta grave se bajarán 10 puntos.\n' +
      'A la segunda falta grave, se le notificará al profesor.']}
  ];
  constructor() { }
  getReglamento(){
    return this.puntos.slice();
  }
}

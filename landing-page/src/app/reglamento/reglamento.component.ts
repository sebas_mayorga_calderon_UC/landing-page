import { Component, OnInit } from '@angular/core';
import {ReglamentoInfoService} from './reglamento-info.service';

@Component({
  selector: 'app-reglamento',
  templateUrl: './reglamento.component.html',
  styleUrls: ['./reglamento.component.css']
})
export class ReglamentoComponent implements OnInit {
 reglamento;
  constructor( private reglamentoService: ReglamentoInfoService) { }

  ngOnInit() {
    this.reglamento= this.reglamentoService.getReglamento();
  }

}

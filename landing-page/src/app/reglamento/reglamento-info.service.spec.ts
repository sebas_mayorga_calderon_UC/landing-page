import { TestBed, inject } from '@angular/core/testing';

import { ReglamentoInfoService } from './reglamento-info.service';

describe('ReglamentoInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReglamentoInfoService]
    });
  });

  it('should be created', inject([ReglamentoInfoService], (service: ReglamentoInfoService) => {
    expect(service).toBeTruthy();
  }));
});
